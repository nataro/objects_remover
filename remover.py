import argparse

import cv2
import numpy


def remove_by_size(
        gray_img: numpy.ndarray,
        lower_size: int = None,
        upper_size: int = None,
        invert: bool = False):
    """
    Parameters
    ----------
    gray_img: numpy.array
        背景が0（黒）、検出する物体が255（白）の画像であること。
        (逆相のときはinvertをTrueにしてください)
        2値化処理したグレーイメージを渡さないと、意図しないノイズが出力画像に残る。

    lower_size : int
        残すブロブの下限のサイズ（面積）

    upper_size: int
        残すブロブの上限のサイズ（面積）

    invert: bool
        入力画像の輝度を反転させる。
        認識領域が逆になっている場合はTrueにする。

    """

    assert gray_img.ndim == 2, 'gray_img must be gray scale image.'

    # 白黒反転処理
    if invert:
        gray_img = cv2.bitwise_not(gray_img)

    # ブロブの検出
    n_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(gray_img)

    # 背景黒の画像を用意
    out_img = numpy.zeros(labels.shape, numpy.dtype('uint8'))

    for i in range(1, n_labels):
        area = stats[i, cv2.CC_STAT_AREA]

        # 上限と下限が設定されている場合
        if (lower_size is not None) and (upper_size is not None):
            if (lower_size < area) and (area < upper_size):
                out_img[labels == i] = 255
            continue

        # 下限のみ設定されている場合
        if (lower_size is not None) and (upper_size is None):
            if lower_size < area:
                out_img[labels == i] = 255
            continue

        # 上限のみ設定されている場合
        if (lower_size is None) and (upper_size is not None):
            if area < upper_size:
                out_img[labels == i] = 255
            continue

    # 白黒反転処理
    if invert:
        out_img = cv2.bitwise_not(out_img)

    return out_img


def parse_option_for_object_remover():
    dc = 'This script is ...'
    parser = argparse.ArgumentParser(description=dc)

    parser.add_argument('-i', action='store', type=str, dest='input',
                        default=None,
                        help='set the path of the original datas directory.  e.g. ./data_src')
    parser.add_argument('-o', action='store', type=str, dest='output',
                        default='output.jpg',
                        help='set the file name to save.  e.g. output.jpg')
    parser.add_argument('-l', '--low', action='store', type=int, dest='low_size',
                        default=None,
                        help='set the lower size threshold.  e.g. 50')
    parser.add_argument('-u', '--upp', action='store', type=int, dest='upp_size',
                        default=None,
                        help='set the upper size threshold.  e.g. 10000')
    parser.add_argument('-r', '--rev', action='store', type=bool, dest='rev',
                        default=True,
                        help='Invert the detected image.  e.g. True')
    return parser.parse_args()


def main():
    args = parse_option_for_object_remover()
    save_file = args.output
    src_file = args.input
    lower_size = args.low_size
    upper_size = args.upp_size
    invert = args.rev

    img = cv2.imread(src_file)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # 2値化しておかないと、connectedComponentsWithStats()実行後にノイズが残る
    _, gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    img_out = remove_by_size(gray, lower_size=lower_size, upper_size=upper_size, invert=invert)

    cv2.imwrite(save_file, img_out)


if __name__ == "__main__":
    """
        remove.pyと同じ階層に解析する画像（下記例ではinput.jpg）を配置した状態で、
        コマンドプロンプトで下記コマンドを実行
    
        サイズの下限のみ指定する場合
        python remover.py -i input.jpg -l 100
        
        下限、上限、検出部の反転を実施する場合
        python remover.py -i input.jpg -l 100 -u 10000 -r True
    """
    main()
